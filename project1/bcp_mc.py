import sys
import numpy as np


class Monomer():
    def __init__(self, m_type="A", x=0, y=0, z=0):
        self.type = m_type
        self.x = x
        self.y = y
        self.z = z


class BCP():
    def __init__(self, formula="A"*4 + "B"*4, bcp_type="A4B4"):
        self.bcp_type = bcp_type
        self.formula = formula
        self.mon_list = []
        self.bcp_len = len(formula)
        self._plymrz()

    def __str__(self):
        return self.formula

    def _plymrz(self):
        for mon in self.formula:
            self.mon_list.append(Monomer(m_type=mon))

    def get_move_cords(self):
        pass  # takes in a move, returns new_cords

    def set_new_cords(self, head=[0,0,0]):
        self.mon_list[0].x, self.mon_list[0].y, self.mon_list[0].z = head
        for i, mon in enumerate(self.mon_list[1:], start=1):
            mon.z = head[-1] + -1*i
        pass  # takes in a move, updates cords


class System():
    def __init__(self, bcp_list, Lx=10, Ly=10, Lz=10):
        self.Lx = Lx
        self.Ly = Ly
        self.Lz = Lz
        self.bcp_list = bcp_list
        self.mon_num = 0
        self._density_check()
        self.sites = np.zeros([Lx, Ly, Lz], np.int32)

    def _density_check(self):
        for bcp in self.bcp_list:
            self.mon_num += bcp.bcp_len
        if self.mon_num > self.Lx * self.Ly:
            print("System is too dense, exiting...")
            sys.exit

    def _system_init(self):
        for bcp in enumerate(self.bcp_list):
            pass  # fills in the system via a raster method

    def _system_mix(self):
            pass  # mixes the system with moves, no energy calc

A = BCP(formula="A"*5)
A.set_new_cords([1,1,3])
print([(mon.x, mon.y, mon.z) for mon in A.mon_list])
A.A_num = 2
#print(A.A_num)
#print(A)
#A.plymrz()
s = System([A])
