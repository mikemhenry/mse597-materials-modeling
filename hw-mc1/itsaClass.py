import numpy as np
import matplotlib.pyplot as plt

class System():

    def init_system(self):
        system = np.zeros(self.num_sites)
        random_sites = np.random.choice(self.num_sites, self.num_particles, replace=False)
        for site in random_sites:
            system[site] = 1
        self.system = system

    def energy(s):
        e = 0
        for i in range(len(s)-1):
            if s[i]==1:
                if s[i+1]==1:
                    e-=1       #-1 energy for each pair of adjacent 1's
        return e

    def trial_move(s): #randomly swap two positions in the system
        temp = np.copy(s)
        p1 = np.random.randint(0,len(s))
        p2 = np.random.randint(0,len(s))
        temp[p2], temp[p1] = temp[p1],temp[p2]
        return temp

    def accept(de,kT): #implement detailed balance
        if np.exp(-de/kT) > np.random.rand():
            return True
        return False

    def run(kT,steps): #specify temperature, number of steps to perform simulation
        system = np.array([1,0]*5) #initialize 1's so they're not touching
        e = np.array( int(steps)*[0])  #store energies in here
        for trials in range(int(steps)):
            trial = trial_move(system)
            de = energy(trial)-energy(system)
            if accept(de,kT):
                system = np.copy(trial)
            e[trials] = energy(system)
        return kT,e.mean(), e.std() #Return the temperature, average energy, and standard deviation

    def num_microstates(n,r):
        return numpy.math.factorial(n)/(numpy.math.factorial(n-r)*numpy.math.factorial(r))

    def __init__(self, num_sites, num_particles):
        self.num_sites = num_sites
        self.num_particles = num_particles
        self.system = self.init_system()

s = System(10,5)
print(s)
